Clone repository.

Copy "ace" script to a /usr/bin, and give it executions permissions.

Usage:

ace start will download and start local server.

ace play acestream://... will play the stream

ace stop to stop server

ace clean to remove docker containers
